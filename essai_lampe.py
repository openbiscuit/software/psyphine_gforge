#!/usr/bin/python
# -*- coding: utf-8 -*-

# Modules à importer
import pypot.robot as pr
from contextlib import closing
import time

## Configuration des moteurs de la lampe
lamp_config = {}
lamp_config['motors'] = {}
lamp_config['motors']['base_rot'] = {
    'id': 5,
    'type': 'AX-18',
    'orientation': 'direct',
    'offset': 0.0,
    'angle_limit': (-90.0, 90.0),
}
lamp_config['motors']['base_arm'] = {
    'id': 4,
    'type': 'AX-18',
    'orientation': 'direct',
    'offset': 0.0,
    'angle_limit': (-10.0, 60.0),
}
lamp_config['motors']['arm'] = {
    'id': 3,
    'type': 'AX-18',
    'orientation': 'direct',
    'offset': 0.0,
    'angle_limit': (5, 80.0), 
    # mais relation entre pos['base_arm'] et max['arm']
}
lamp_config['motors']['head_arm'] = {
    'id': 2,
    'type': 'AX-12',
    'orientation': 'direct',
    'offset': 0.0,
    'angle_limit': (-15.0, 120.0),
}

lamp_config['motorgroups'] = {
    'base' : ['base_rot','base_arm'],
    'mid' : ['arm'],
    'head' : ['head_arm']
}

lamp_config['controllers'] = {}
lamp_config['controllers']['lamp_controler'] = {
    'port': '/dev/ttyACM0',
    'sync_read': False,
    'attached_motors' : ['base','mid','head'],
    'protocol': 1
}

## lamp = pr.from_config( lamp_config )

# ******************************************************************** motors_on
def motors_on( lamp ) :
    """ Rend tous les moteurs non-compliant"""
    for m in lamp.motors:
        m.compliant = False
# ******************************************************************* motors_off
def motors_off( lamp ) :
    """ Rend tous les moteurs compliant """
    for m in lamp.motors:
        m.compliant = True
# ************************************************************ motors_properties
def motors_properties( lamp ):
    """
    """
    # instant de début
    start_time = time.time()
    
    # Moteur compliant
    for m in lamp.motors:
        m.compliant = True

    # Pendant 10 secondes
    while (time.time()-start_time < 10 ):
        # Les positions des moteurs
        for m in lamp.motors:
            print( "["+str(m.id)+"] "+m.name)
            print( "   _compliant    : " + str(m.compliant) )
            print( "   _present_pos  : " + str(m.present_position) )
            print( "   _goal_pos     : " + str(m.goal_position) )
            print( "   _present_spd  : " + str(m.present_speed) )
            print( "   _moving_spd   : " + str(m.moving_speed) )
            print( "   _goal_spd     : " + str(m.goal_speed) )
        # We want to run this loop at 10Hz.
        time.sleep(0.1)
    

# # ************************************************************************* MAIN
# if __name__ == '__main__':
#     # L'utilisation de 'closing' garanti que la fonction 'close()' sera bien
#     # appelée dans tous les cas => bonne déconnexion de la lampe
#     with closing( pr.from_config( lamp_config )) as lamp:
#         motors_properties( lamp )

# goto( dict(mot_name:pos), time )
# lamp.goto_position( {'base_rot':60, 'base_arm':30}, 2)
