# Expérience PsyPhINe

Code et ressources autour des expérimentations du projet PsyPhINe.

## Pypot et VREP
* pense_bete.org contient des liens et quelques infos sur l'installation de Pypot et VREP
* essai_lampe.py : code en python pour essayer Pypot sur la lampe
* psyphine_lamp.ipynb : explication sur l'utilisation de Pypot en utilisant les notebook de ipython (jupyter)

